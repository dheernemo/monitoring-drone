///// esp8266 /////
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "OPPO A3s";
const char* password = "12345678";
const char* mqtt_server = "103.60.180.22";

WiFiClient espClient;
PubSubClient client(espClient);

String msg;
static char temp[20];

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.subscribe("drone");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
///// esp8266 /////

///// gps /////
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

static const int TXPin = 0, RXPin = 2;
static const uint32_t Baud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(TXPin, RXPin);
///// gps /////

///// dht22 /////
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 14     // Digital pin connected to the DHT sensor 
#define DHTTYPE    DHT22     // DHT 22 (AM2302)

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;
///// dht22 /////

///// bmp180 /////
#include <Wire.h>
#include <Adafruit_BMP085.h>

#define I2C_SCL 5
#define I2C_SDA 4
Adafruit_BMP085 bmp;
///// bmp180 /////

void setup()
{
  Serial.begin(Baud);
  ss.begin(Baud);

  dht.begin();

//  bmp.begin();

  if (!bmp.begin()) {
  Serial.println("Could not find a valid BMP085 sensor, check wiring!");
  while (1) {}
  }

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  
}

void loop()
{
  // This sketch displays information every time a new sentence is correctly encoded.
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while(true);
  }
}

void displayInfo()
{

  ///// esp8266 /////////
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  ///// esp8266 /////////

  ///// datetime gps /////////
//  if (gps.date.isValid())
//  {
//    Serial.print(gps.date.year());
//    Serial.print(F("/"));
//    if (gps.date.month() < 10) Serial.print(F("0"));
//    Serial.print(gps.date.month());
//    Serial.print(F("/"));
//    if (gps.date.day() < 10) Serial.print(F("0"));
//    Serial.print(gps.date.day());
//  }
//  else
//  {
//    Serial.print(F("INVALID"));
//  }
  msg = "";
  if (gps.date.isValid())
  {
    msg += gps.date.year();
    msg += "/";
    if (gps.date.month() < 10) msg += "0";
    msg += gps.date.month();
    msg += "/";
    if (gps.date.day() < 10) msg += "0";
    msg += gps.date.day();
  }
  else
  {
    msg += "INVALID";
  }

//  Serial.print(F(" "));
//  if (gps.time.isValid())
//  {
//    if (gps.time.hour() < 10) Serial.print(F("0"));
//    Serial.print(gps.time.hour());
//    Serial.print(F(":"));
//    if (gps.time.minute() < 10) Serial.print(F("0"));
//    Serial.print(gps.time.minute());
//    Serial.print(F(":"));
//    if (gps.time.second() < 10) Serial.print(F("0"));
//    Serial.print(gps.time.second());
//  }
//  else
//  {
//    Serial.print(F("INVALID"));
//  }

  msg += " ";
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) msg += "0";
    msg += gps.time.hour();
    msg += ":";
    if (gps.time.minute() < 10) msg += "0";
    msg += gps.time.minute();
    msg += ":";
    if (gps.time.second() < 10) msg += "0";
    msg += gps.time.second();
  }
  else
  {
    msg += "INVALID";
  }
  ///// datetime gps /////////
  
  ///// temperature dht22 /////////
//  Serial.print(F(","));
//  sensors_event_t event;
//  dht.temperature().getEvent(&event);
//  if (isnan(event.temperature)) {
//    Serial.println(F("0"));
//  }
//  else {
//    Serial.print(event.temperature);
//  }
  msg += ",";
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    msg += "0";
  }
  else {
    msg += event.temperature;
  }
  ///// temperature dht22 /////////

  ///// humid dht22 /////////
  msg += ",";
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    msg += "0";
  }
  else {
    msg += event.relative_humidity;
  }
//  Serial.print(F(","));
//  dht.humidity().getEvent(&event);
//  if (isnan(event.relative_humidity)) {
//    Serial.println(F("0"));
//  }
//  else {
//    Serial.print(event.relative_humidity);
//  }
  ///// humid dht22 /////////

  ///// pressure bmp180 /////////
//  Serial.print(",");
//  Serial.print(bmp.readPressure() * 0.01);
  msg += ",";
  msg += bmp.readPressure() * 0.01;
  ///// pressure bmp180 /////////

  ///// altitude bmp180 /////////
//  Serial.print(",");
//  Serial.print(bmp.readAltitude(101500));
  msg += ",";
  msg += (bmp.readAltitude(100880) - 11);
//  msg += gps.altitude.meters();
  ///// altitude bmp180 /////////

  ///// longlat gps /////////
//  Serial.print(F(",")); 
//  if (gps.location.isValid())
//  {
//    Serial.print(gps.location.lat(), 6);
//    Serial.print(F(","));
//    Serial.print(gps.location.lng(), 6);
//  }
//  else
//  {
//    Serial.print(F("INVALID"));
//  }
  
  msg += ",";
  if (gps.location.isValid())
  {
    dtostrf(gps.location.lat(),7, 6, temp);
    msg += temp;
    msg += ",";
    dtostrf(gps.location.lng(),7, 6, temp);
    msg += temp;
  }
  else
  {
    msg += "INVALID";
  }
  ///// longlat gps /////////

  Serial.println(msg);
  char sending[100];
  msg.toCharArray(sending, 100);
  client.publish("drone", sending);
  delay(500);
}
